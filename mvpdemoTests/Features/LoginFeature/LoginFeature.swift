//
//  LoginFeature.swift
//  mvpdemoTests
//
//  Created by arodriguez on 10-03-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import XCTest
@testable import mvpdemo
class LoginFeature: XCTestCase {

    
    let model: LoginModel = {
        let service = MockupLoginService()
        let model = LoginModel(service)
        return model
    }()
    
    lazy var presenter: LoginPresenter = {
        let service = MockupLoginService()
        let model = LoginModel(service)
        let presenter = LoginPresenter(model)
        presenter.view = self
        return presenter
    }()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        UserStorage.logout()
    }

    // MARK: - Model
    
    func test_loginOK() {
        let exp = expectation(description: "OK")
        self.model.getLogin(email: "a@a.cl", password: "123123123") { (state: LoginState) in
            XCTAssert(state == .OK)
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 4)
        XCTAssert(self.model.user != nil)
    }
    
    func test_loginERROR() {
          let exp = expectation(description: "OK")
          self.model.getLogin(email: "a@a.cl2", password: "123123123") { (state: LoginState) in
              XCTAssert(state == .ERROR)
              exp.fulfill()
          }
          
          wait(for: [exp], timeout: 4)
          XCTAssert(self.model.user == nil)
      }
     
    // MARK: - PRESENTER
    var exppe: XCTestExpectation?
    
    func test_callserviceOK() {
        exppe = expectation(description: "OK")
        self.presenter.getLogin(email: "a@a.cl", password:  "123123123")
        wait(for: [self.exppe!], timeout: 4)
        XCTAssert(self.exppe?.description == "ACK")
    }
    
    func test_callserviceERROR() {
        exppe = expectation(description: "OK")
        self.presenter.getLogin(email: "a@a.cle", password:  "123123123")
        wait(for: [self.exppe!], timeout: 4)
        XCTAssert(self.exppe?.description == "NACK")
    }
    
}

// MARK: - View
extension LoginFeature: LoginDisplayLogic {
    func loginOK() {
        self.exppe?.expectationDescription = "ACK"
        self.exppe?.fulfill()
    }
    
    func loginERROR() {
        self.exppe?.expectationDescription = "NACK"
        self.exppe?.fulfill()
    }
}
