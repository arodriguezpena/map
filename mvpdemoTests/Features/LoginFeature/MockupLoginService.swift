//
//  MockupLoginService.swift
//  mvpdemoTests
//
//  Created by arodriguez on 17-03-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import Foundation
@testable import mvpdemo

class MockupLoginService: LoginService {
    override func getLogin(email: String, password: String, callback: @escaping (LoginResponse?) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if email != "a@a.cl" || password != "123123123" {
               return callback(LoginResponse(success: false, payload: nil))
            }
                       
           let req = LoginResponse(success: true, payload: UserEntity(email: "a@a.cl", firstName: "Alejandro", lastName: "Rodriguez", avatar: "https://media-exp1.licdn.com/dms/image/C5103AQHuVFunliEC8Q/profile-displayphoto-shrink_200_200/0?e=1589414400&v=beta&t=Cbn2uGsiHVnGguUYWVGaf-esuEG6yeLBnudvRjJ3Nko"))
           
           callback(req)
        }
    }
}
