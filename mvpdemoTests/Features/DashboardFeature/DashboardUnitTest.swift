//
//  DashboardUnitTest.swift
//  mvpdemoTests
//
//  Created by arodriguez on 17-03-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import XCTest
@testable import mvpdemo
class DashboardUnitTest: XCTestCase {

    var presenter: DashboardPresenter!
    
    var user =  UserEntity(email: "a@a.cl", firstName: "Alejandro", lastName: "Rodriguez", avatar: "")
    override func setUp() {
        UserStorage.setUser(user)
        let service = DashboardService()
        let model = DashboardModel(service)
        self.presenter = DashboardPresenter(model)
    }
    
    override class func tearDown() {
        
    }
    
    func test_getUser() {
        XCTAssert(self.presenter.getUser().email == user.email)
    }
    func test_getUserFatalError() {
        UserStorage.logout()
        expectFatalError(expectedMessage: "Feature - Dashboard: LocalStorage not saved!") {
            let service = DashboardService()
            let model = DashboardModel(service)
        }
    }

}
