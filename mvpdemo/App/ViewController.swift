//
//  ViewController.swift
//  mvpdemo
//
//  Created by arodriguez on 09-03-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func tapLogin(_ sender: Any) {
        let vd = LoginFactory().getLoginViewController()
        self.present(vd, animated: true, completion: nil)
    }
    
}

