//
//  Router.swift
//  mvpdemo
//
//  Created by arodriguez on 10-03-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import Foundation
import UIKit

/// Ruter: call to features
class Router {
    static let shared = Router()
    func getInitialVC() -> UIViewController {
        if UserStorage.currentUser() == nil {
            return self.getLoginVC()
        } else {
            return self.getDashboardVC()
        }
    }
}

// MARK: - Login and Logout Feature
extension Router {
    func getLoginVC() -> UIViewController {
        let login = LoginFactory().getLoginViewController()
        return login
    }

    func logout() {
        UserStorage.logout()
        guard let window = UIApplication.shared.windows.first else {
            return
        }

        window.rootViewController = UINavigationController(rootViewController: self.getLoginVC())
        window.makeKeyAndVisible()
        UIView.transition(with: window,
        duration: 0.3,
        options: .transitionCrossDissolve,
        animations: nil,
        completion: nil)
    }
}
// MARK: - Feature Dashboard
extension Router {
    func getDashboardVC() -> UIViewController {
        return DashboardFactory().getDashboardViewController()
    }
}
