//
//  LoginService.swift
//  mvpdemo
//
//  Created by arodriguez on 09-03-20.
//  Copyright (c) 2020 IONIX. All rights reserved.

import UIKit

struct UserEntity: Codable {
    let email: String
    let firstName: String
    let lastName: String
    let avatar: String?
    
    func getImage() -> UIImage? {
        return nil
    }
    func getFullName () -> String {
        return "\(self.firstName) \(self.lastName)"
    }
}

protocol LoginServiceLogic: class {
    func getLogin(email: String, password: String, callback: @escaping (IResponse<UserEntity?>) -> Void)
}

class LoginService: LoginServiceLogic {
    
    func getLogin(email: String, password: String, callback: @escaping (IResponse<UserEntity?>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute:  {
            
            if email != "a@a.cl" || password != "123123123" {
                return callback(IResponse(responseCode: "401", description: "El usuario es invalido", result: nil))
            }
            //decode try
            let user = UserEntity(email: "a@a.cl", firstName: "Alejandro", lastName: "Rodriguez", avatar: "https://media-exp1.licdn.com/dms/image/C5103AQHuVFunliEC8Q/profile-displayphoto-shrink_200_200/0?e=1589414400&v=beta&t=Cbn2uGsiHVnGguUYWVGaf-esuEG6yeLBnudvRjJ3Nko")
            
                
            callback(IResponse(responseCode: "OK", description: nil, result: user))
        })
    }
}
