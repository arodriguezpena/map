//
//  LoginModel.swift
//  mvpdemo
//
//  Created by arodriguez on 09-03-20.
//  Copyright (c) 2020 IONIX. All rights reserved.

import UIKit


enum LoginState {
    case OK
    case ERROR
}

protocol LoginModelLogic {
    func getLogin(email: String, password: String, callback: @escaping (LoginState) -> Void)
}

protocol LoginDataStore {
	var user: UserEntity? { get set }
    var error: String? { get set }
    var code: String? {get set}
}

class LoginModel: LoginModelLogic, LoginDataStore {
	var service: LoginService
    
    var error: String?
    var code: String?
    
    var user: UserEntity? {
        didSet {
            if let user = user {
               UserStorage.setUser(user)
            }
        }
    }
  
	init(_ service: LoginService) {
		self.service = service
	}
    
    func getLogin(email: String, password: String, callback: @escaping (LoginState) -> Void) {
        
        APIManager.shared.get(url: "") { (req: IRequest<UserEntity>) in
        
        }
        self.service.getLogin(email: email, password: password) { [weak self] req in
            
            if req.success() {
                guard let user = req.result else {
                    return callback(.ERROR)
                }
                self?.user = user
                callback(.OK)
            } else {
                //errroes
                self?.code = req.responseCode
                self?.error = req.description
                callback(.ERROR)
            }
        }
    }
}
