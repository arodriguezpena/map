//
//  LoginViewController.swift
//  mvpdemo
//
//  Created by arodriguez on 09-03-20.
//  Copyright (c) 2020 IONIX. All rights reserved.

import UIKit
import FlexLayout
import PinLayout

protocol LoginDisplayLogic: class {
    func loginOK()
    func loginERROR()
}

class LoginViewController: UNXViewController  {
	var presenter: LoginPresentationLogic!
    
    // MARK: - Properties
    let email = INXTextField(placeholder: "Ingrese email", errorLabel: "El Email es invalido", type: .email)
    let password = INXTextField(placeholder: "Ingrese su contraña", errorLabel: "Ingrese una contraseña valida", type: .password)
    let loginButton = INXButton(title: "Login", style: .purple, target: self, selector: #selector(didTapLogin))

    // MARK: - View lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.setupView()
	}

    override func viewDidLayoutSubviews() {
         super.viewDidLayoutSubviews()
         self.view.flex.layout()
    }

    // MARK: - Actions
    @objc func didTapLogin(_ sender: Any) {
        if email.validOrFocus() && password.validOrFocus() {
            self.navigationController?.showLoading()
            self.presenter.getLogin(email: email.text, password: password.text)
        } else {
            self.alertMessage(title: "ups", message: "Debe ingresar los campos correctamente")
        }
    }
}
// MARK: - SetupView FlexBox
extension LoginViewController {
    private func setupView() {
        guard let container = self.view else {
            return
        }
        container.backgroundColor = .white
        container.flex.justifyContent(.center).padding(20)
            .define { flex in
            
                flex.addItem().height(300).define { lc in
                    lc.addItem(email)
                    lc.addItem(password)
                    lc.addItem(loginButton)
            }
        }
    
    }
}


// MARK: - Response Services
extension LoginViewController: LoginDisplayLogic {
    func loginOK() {
        self.navigationController?.hideLoading()
        self.navigationController?.pushViewController(self.router.getDashboardVC(), animated: true)
    }
    func loginERROR() {
        self.navigationController?.hideLoading()
        self.alertMessage(title: "Ups", message: "Usuario o password invalido")
    }

}
