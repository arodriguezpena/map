//
//  LoginPresenter.swift
//  mvpdemo
//
//  Created by arodriguez on 09-03-20.
//  Copyright (c) 2020 IONIX. All rights reserved.

import Foundation

protocol LoginPresentationLogic: class {
    func getLogin(email: String, password: String)
}

class LoginPresenter: LoginPresentationLogic {
	
	weak var view: LoginDisplayLogic?
	var model: LoginModelLogic
	var dataStore: LoginDataStore

	init(_ model: LoginModel) {
		self.model = model 
		self.dataStore = model
	}
    
    func getLogin(email: String, password: String) {
        self.model.getLogin(email: email, password: password) { [weak self] state in
            guard let self = self, let view = self.view else {
                return
            }
            if state == .OK {
                view.loginOK()
            } else {
              
                view.loginERROR()
            }
        }
    }
}
