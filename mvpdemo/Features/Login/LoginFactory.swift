//
//  LoginFactory.swift
//  mvpdemo
//
//  Created by arodriguez on 09-03-20.
//  Copyright (c) 2020 IONIX. All rights reserved.

import Foundation
import UIKit
public class LoginFactory {

    public func getLoginViewController() -> UIViewController {
        let service = LoginService()
        let model = LoginModel(service)
        let presenter = LoginPresenter(model)
        let viewController = LoginViewController()

        viewController.presenter = presenter
        presenter.view = viewController
        return viewController
    }
}