//
//  DashboardFactory.swift
//  mvpdemo
//
//  Created by arodriguez on 10-03-20.
//  Copyright (c) 2020 IONIX. All rights reserved.

import Foundation
import UIKit
public class DashboardFactory {

    public func getDashboardViewController() -> UIViewController {
        let service = DashboardService()
        let model = DashboardModel(service)
        let presenter = DashboardPresenter(model)
        let viewController = DashboardViewController()

        viewController.presenter = presenter
        presenter.view = viewController
        return viewController
    }
}