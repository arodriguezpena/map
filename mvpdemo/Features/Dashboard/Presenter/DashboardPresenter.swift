//
//  DashboardPresenter.swift
//  mvpdemo
//
//  Created by arodriguez on 10-03-20.
//  Copyright (c) 2020 IONIX. All rights reserved.

import Foundation

protocol DashboardPresentationLogic: class {
    func getUser() -> UserEntity
}

class DashboardPresenter: DashboardPresentationLogic {
	
	weak var view: DashboardDisplayLogic?
	var model: DashboardModelLogic
	var dataStore: DashboardDataStore

	init(_ model: DashboardModel) {
		self.model = model 
		self.dataStore = model
	}
    
    func getUser() -> UserEntity {
        return self.dataStore.user
    }
}
