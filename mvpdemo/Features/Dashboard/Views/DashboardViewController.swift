//
//  DashboardViewController.swift
//  mvpdemo
//
//  Created by arodriguez on 10-03-20.
//  Copyright (c) 2020 IONIX. All rights reserved.

import UIKit
import FlexLayout
import PinLayout

protocol DashboardDisplayLogic: class {
}

class DashboardViewController: UNXViewController, DashboardDisplayLogic {

    // MARK: - Properties
    var presenter: DashboardPresentationLogic!
    var fullName = UILabel()
    var email = UILabel()
    var button = INXButton(title: "Logout", style: .purple)
   
    // MARK: - View lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
        self.setupView()
        self.setupData()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.flex.layout()
    }
    func setupData() {
        let user = presenter.getUser()
        self.fullName.set(contents: [.text("Hola, \(user.firstName).", style: .h3, textColor: nil, textAlignment: .center, insertNewLine: false)])
        self.email.set(contents: [.text("\(user.email)", style: .h6, textColor: nil, textAlignment: .center, insertNewLine: false)])
    }
    
    // MARK: - Actions
    @objc func didTapLogout() {
        self.router.logout()
    }
}

//MARK: - FlexLayout
extension DashboardViewController {
    func setupView() {
        self.view.backgroundColor = .hex("#C3C3C3")
        self.view.flex.define { flex in
            flex.addItem()
                .backgroundColor(.yellow)
                .alignItems(.center)
                .height(35%).define { flex in
                    flex.addItem().height(80%).width(70%).marginTop(40%)
                    .alignItems(.center)
                    .paddingLeft(16)
                    .paddingRight(16)
                    .justifyContent(.center)
                    .backgroundColor(.white).define { flex in
                        flex.view?.layer.cornerRadius = 16
                        flex.addItem(self.fullName).padding(0)
                        flex.addItem(self.email).padding(0)
                        self.button.addTarget(self, action: #selector(didTapLogout), for: .touchUpInside)
                        flex.addItem(self.button).marginTop(8)
                    }
                }
            }
           
    }
}
