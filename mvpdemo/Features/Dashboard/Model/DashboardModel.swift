//
//  DashboardModel.swift
//  mvpdemo
//
//  Created by arodriguez on 10-03-20.
//  Copyright (c) 2020 IONIX. All rights reserved.

import UIKit
protocol DashboardModelLogic {
}

protocol DashboardDataStore {
	var user: UserEntity { get set }
}

class DashboardModel: DashboardModelLogic, DashboardDataStore {
	var service: DashboardService
	var user: UserEntity
	
	init(_ service: DashboardService) {
		self.service = service
        guard let user = UserStorage.currentUser() else {
            fatalError("DashboardModel: I don't have the current user")
        }
        self.user = user
	}
}
