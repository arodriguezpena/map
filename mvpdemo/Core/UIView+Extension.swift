//
//  UIView+Extension.swift
//  mvpdemo
//
//  Created by arodriguez on 09-03-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import Foundation
import UIKit
var vSpinner : UIView?
extension UIViewController {
    func alertMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let accept = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(accept)
        self.present(alert, animated: true)
    }
    
    // MARK: - Acitivity Indicator
    
    func showLoading() {
        self.view.endEditing(true)
        let spinnerView = UIView.init(frame: self.view.bounds)
          spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .large)
          ai.startAnimating()
          ai.center = spinnerView.center
          
          DispatchQueue.main.async {
              spinnerView.addSubview(ai)
            self.view.addSubview(spinnerView)
          }
      
      vSpinner = spinnerView
    }
  
  func hideLoading() {
      DispatchQueue.main.async {
          vSpinner?.removeFromSuperview()
          vSpinner = nil
      }
  }
}
