//
//  UserStorage.swift
//  mvpdemo
//
//  Created by arodriguez on 09-03-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import Foundation

private enum UserEnum {
    static let email = "emailz"
    static let avatar = "avatar"
    static let firstName = "first_name"
    static let lastName = "last_name"
}
final class UserStorage {
    
    /// Get UserEntity
    static func currentUser() -> UserEntity? {
        if let email = UserDefaults.standard.string(forKey: UserEnum.email),
            let avatar = UserDefaults.standard.string(forKey: UserEnum.avatar),
            let firstName = UserDefaults.standard.string(forKey: UserEnum.firstName),
            let lastName = UserDefaults.standard.string(forKey: UserEnum.lastName) {
            return UserEntity(email: email, firstName: firstName, lastName: lastName, avatar: avatar)
        }
        return nil
    }
    /// Set User in storage
    /// - Parameter user: userentity
    static func setUser(_ user: UserEntity) {
        UserDefaults.standard.setValue(user.email, forKey: UserEnum.email)
        UserDefaults.standard.setValue(user.avatar, forKey: UserEnum.avatar)
        UserDefaults.standard.setValue(user.firstName, forKey: UserEnum.firstName)
        UserDefaults.standard.setValue(user.lastName, forKey: UserEnum.lastName)
    }
    static func logout() {
       
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
           defaults.removeObject(forKey: key)
        }
    }
    
}
