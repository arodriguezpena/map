//
//  ApiManager.swift
//  mvpdemo
//
//  Created by arodriguez on 23-03-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import Foundation

struct IRequest<T: Codable>: Codable {
    let code: String
    let message: String?
    let payload: T?
    
    func success() -> Bool {
        return self.code == "200" ? true : false
    }
}



/// Api Managert Protocol
protocol APIManagerProtocol {
    /// Get Request
    /// - Parameters:
    ///   - url: url Strinh
    ///   - completion: IRequest
    func get<T>(url: String,  completion: @escaping (IRequest<T>) -> Void)
    /// POST Request
    /// - Parameters:
    ///   - url: url String
    ///   - data: body Post
    ///   - completion: IRequest
    func post<T>(url: String, data: NSDictionary, completion: @escaping (IRequest<T>) -> Void)
}


class APIManager: APIManagerProtocol {
    static let shared: APIManagerProtocol = APIManager()

    func get<T>(url: String,  completion: @escaping (IRequest<T>) -> Void) {
        guard let uri = URL(string: url) else {
            let req = IRequest<T>(code: "500", message: "Url Invalida", payload: nil)
            return completion(req)
        }
        DispatchQueue.global(qos: .background).async {
            self.task(request: URLRequest(url: uri), completion: completion).resume()
        }
    }
    
    func post<T>(url: String, data: NSDictionary, completion: @escaping (IRequest<T>) -> Void) {
        guard let uri = URL(string: url) else {
            let req = IRequest<T>(code: "500", message: "Url Invalida", payload: nil)
            return completion(req)
        }
        var request = URLRequest(url: uri)
        request.httpMethod = "POST"

        DispatchQueue.global(qos: .background).async {
            self.task(request: URLRequest(url: uri), completion: completion).resume()
        }
    }
}

extension APIManager {
    private func task<T>(request: URLRequest, completion: @escaping (IRequest<T>) -> Void) -> URLSessionDataTask {
        return  URLSession.shared.dataTask(with: request) { data, response, err in
           guard let data = data, err == nil else {
               print("error=\(String(describing: err?.localizedDescription))")
               let req = IRequest<T>(code: "500", message: err?.localizedDescription, payload: nil)
               return completion(req)
           }
           
           do {
               let json = try JSONDecoder().decode(IRequest<T>.self, from: data)
               return completion(json)
           }catch {
               let req = IRequest<T>(code: "500", message: "Error 500", payload: nil)
               return completion(req)
           }
        }
        
    }
}
