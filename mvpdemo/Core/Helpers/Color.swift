//
//  Color.swift
//  OnePay
//
//  Created by fyarad on 06-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit

extension UIColor {

    static var purpleColor = UIColor.hex("#3F1B7B")
    static var purpleGradientStart = UIColor.hex("#3F1B7B")
    static var purpleGradientEnd = UIColor.hex("#3F1B7B")

    static var yellowColor = UIColor.hex("#f9ad25")
    static var yellowGradientStart = UIColor.hex("#FFC15E")
    static var yellowGradientEnd = UIColor.hex("#FFA818")

    static var greenColor = UIColor.hex("#19A419")
    static var redColor = UIColor.hex("#E43935")

    static var whiteLight = UIColor.white.withAlphaComponent(0.6)
    static var blackLight = UIColor.black.withAlphaComponent(0.4)

    static var dynamicLabel: UIColor = UIColor.darkGray
    static var dynamicLabelSecondary: UIColor = UIColor.gray
    static var dynamicLabelTertiary: UIColor = UIColor.lightGray
    
    static var background: UIColor = .white
    static var backgroundLight: UIColor = UIColor.hex("#F5F5F5")
    static var divider: UIColor = UIColor.hex("#E0E0E0")
    
    static func hex(_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    static func image(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) -> UIImage? {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }


}
