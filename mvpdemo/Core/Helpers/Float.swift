//
//  Float.swift
//  OnePay
//
//  Created by fyarad on 06-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit

extension CGFloat {
    
    static var paddingSmall = CGFloat(6.0)
    static var padding = CGFloat(12.0)
    static var paddingMedium = CGFloat(18.0)
    static var paddingBig = CGFloat(24.0)

    static var screenWidth = UIScreen.main.bounds.size.width
    static var screenHeight = UIScreen.main.bounds.size.height
    static var statusBarHeight = UIApplication.shared.statusBarFrame.size.height
    static var navbarHeight = CGFloat(44)
    static var topHeight = CGFloat.statusBarHeight + CGFloat.navbarHeight
    
    static var bottomSafe = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        
}
