//
//  IResponse.swift
//  mvpdemo
//
//  Created by arodriguez on 19-03-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import Foundation

struct IResponse<T> {
    let responseCode: String // OK  != MAL
    let description: String?
    let result: T?
    
    func success() -> Bool  {
        return responseCode == "OK" ? true: false
    }
    
    
}
