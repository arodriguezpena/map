//
//  INXModalView.swift
//  OnePay
//
//  Created by fyarad on 08-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit
import FlexLayout

class INXModalView: UIView, UIGestureRecognizerDelegate {
    
    var pan: UIPanGestureRecognizer?
    var tap: UITapGestureRecognizer?
    var contentView: UIView = UIView()
    var containerView: UIView = UIView()
    var dismissing = false
    var keyboardHeight = CGFloat(0)
    
    public var indicatorView = INXAlertIndicatorView()
    private let backgroundView = UIView()
    private var feedbackGenerator: UIImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
    
    private var workGester: Bool = false
    private var startDismissing: Bool = false
    private var afterReleaseDismissing: Bool = false
    
    
    private var animationDamping: CGFloat = 0.9
    private var animationVelocity: CGFloat = 0.9
    
    private var translateForDismiss: CGFloat = 160
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: .screenWidth, height: .screenHeight))
        
        self.feedbackGenerator.prepare()
        
        let backgroundTap = UITapGestureRecognizer(target: self, action: #selector(dismissTap))
        backgroundTap.cancelsTouchesInView = false
        self.backgroundView.addGestureRecognizer(backgroundTap)
        
        self.backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.backgroundView.alpha = 0.0;
        self.contentView.backgroundColor = .background
        
        self.indicatorView.sizeToFit()
        self.indicatorView.color = .background
        
        self.flex.position(.absolute).width(100%).height(100%).top(0).bottom(0).define { (flex) in
            flex.addItem(self.backgroundView).position(.absolute).width(100%).height(100%).top(0).bottom(0)
            
            flex.addItem(self.containerView).position(.absolute).width(100%).alignItems(.center).justifyContent(.start).top(.screenHeight).define({ (flex) in
                flex.addItem(self.indicatorView).marginBottom(.padding)
                flex.addItem(self.contentView)
            })
            
        }
        self.flex.layout()
        
        
        self.pan = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan))
        self.pan?.delegate = self
        self.pan?.maximumNumberOfTouches = 1
        self.pan?.cancelsTouchesInView = false
        if let panGesture = self.pan {
            self.addGestureRecognizer(panGesture)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.flex.layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func show() {
        
        self.containerView.flex.top(.screenHeight)
        self.indicatorView.style = .arrow
        self.backgroundView.alpha = 0.0
        self.flex.layout()
        
        UIApplication.shared.keyWindow?.addSubview(self)
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            usingSpringWithDamping: animationDamping,
            initialSpringVelocity: animationVelocity,
            options: .curveEaseOut,
            animations: {
                self.backgroundView.alpha = 1.0
                self.setContentViewOffset(offset: 0)
        }, completion: { finished in
            
        })
        
    }
    
    func setContentViewOffset(offset: CGFloat) {
        
        let finish = CGFloat.screenHeight
        let start = finish - self.containerView.height
        
        self.backgroundView.alpha = 1.0 - ((offset / 100) * (finish - start))
        self.containerView.flex.top(start + offset - self.keyboardHeight)
        
        self.flex.layout()
    }
    
    @objc func dismissTap() {
        self.dismiss()
    }
    
    @objc func dismiss(completionBlock: (() -> Void)? = nil) {
        
        self.indicatorView.style = .line
        self.willDismiss()
        
        UIView.animate(
            withDuration: 0.9,
            delay: 0,
            usingSpringWithDamping: animationDamping,
            initialSpringVelocity: animationVelocity,
            options: .curveEaseOut,
            animations: {
                self.setContentViewOffset(offset: self.containerView.frame.origin.y)
        }, completion: { finished in
            self.removeFromSuperview()
            self.didDismiss()
            completionBlock?()
        })
        
    }
    
    func willDismiss() {
        self.endEditing(true)
        self.dismissing = true
    }
    
    func didDismiss() {
        self.dismissing = false
    }
    
    @objc func handlePan(gestureRecognizer: UIPanGestureRecognizer) {
        
        if (self.dismissing) {
            return
        }
        
        let translation = gestureRecognizer.translation(in: self).y
        
        switch gestureRecognizer.state {
        case .began:
            self.workGester = true
            self.layer.removeAllAnimations()
            self.endEditing(true)
            self.superview?.endEditing(true)
            gestureRecognizer.setTranslation(CGPoint(x: 0, y: 0), in: self)
        case .changed:
            self.workGester = true
            self.updatePresentedViewForTranslation(inVerticalDirection: translation)
            self.indicatorView.style = (translation >= self.translateForDismiss) ? .line : .arrow
        case .ended:
            self.workGester = false
            if translation >= self.translateForDismiss {
                self.dismiss()
            }
            else {
                self.indicatorView.style = .arrow
                UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [.curveEaseOut, .allowUserInteraction], animations: {
                    self.setContentViewOffset(offset: 0)
                })
            }
        default:
            break
        }
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let gester = gestureRecognizer as? UIPanGestureRecognizer {
            let velocity = gester.velocity(in: self)
            return abs(velocity.y) > abs(velocity.x)
        }
        return true
    }
    
    private func updatePresentedViewForTranslation(inVerticalDirection translation: CGFloat) {
        if self.startDismissing { return }
        
        let elasticThreshold: CGFloat = 120
        let translationFactor: CGFloat = 0.5
        
        if translation >= 0 {
            let translationForModal: CGFloat = {
                if translation >= elasticThreshold {
                    let frictionLength = translation - elasticThreshold
                    let frictionTranslation = 30 * atan(frictionLength / 120) + frictionLength / 10
                    return frictionTranslation + (elasticThreshold * translationFactor)
                } else {
                    return translation * translationFactor
                }
            }()
            
            self.setContentViewOffset(offset: translationForModal)
            
            let gradeFactor = 1 + (translationForModal / 7000)
            self.backgroundView.alpha = self.alpha - ((gradeFactor - 1) * 15)
            
        }
        
        let afterRealseDismissing = (translation >= self.translateForDismiss)
        if afterRealseDismissing != self.afterReleaseDismissing {
            self.afterReleaseDismissing = afterRealseDismissing
            self.feedbackGenerator.impactOccurred()
        }
        
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            self.keyboardHeight = keyboardFrame.cgRectValue.height
            self.setContentViewOffset(offset: 0)
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.keyboardHeight = 0
        self.setContentViewOffset(offset: 0)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

open class INXAlertIndicatorView: UIView {
    
    var style: Style = .line {
        didSet {
            switch self.style {
            case .line:
                self.animate {
                    self.leftView.transform = .identity
                    self.rightView.transform = .identity
                }
            case .arrow:
                self.animate {
                    let angle = CGFloat(20 * Float.pi / 180)
                    self.leftView.transform = CGAffineTransform.init(rotationAngle: angle)
                    self.rightView.transform = CGAffineTransform.init(rotationAngle: -angle)
                }
            }
            
        }
    }
    
    var color: UIColor = UIColor.init(red: 202/255, green: 201/255, blue: 207/255, alpha: 1) {
        didSet {
            self.leftView.backgroundColor = self.color
            self.rightView.backgroundColor = self.color
        }
    }
    
    private var leftView: UIView = UIView()
    private var rightView: UIView = UIView()
    
    init() {
        super.init(frame: .zero)
        self.backgroundColor = UIColor.clear
        self.addSubview(self.leftView)
        self.addSubview(self.rightView)
        self.color = UIColor.init(red: 202/255, green: 201/255, blue: 207/255, alpha: 1)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func sizeToFit() {
        super.sizeToFit()
        self.frame = CGRect.init(x: self.frame.origin.x, y: self.frame.origin.y, width: 36, height: 13)
        
        let height: CGFloat = 5
        let correction = height / 2
        
        self.leftView.frame = CGRect.init(x: 0, y: 0, width: self.frame.width / 2 + correction, height: height)
        self.leftView.center.y = self.frame.height / 2
        self.leftView.layer.cornerRadius = min(self.leftView.frame.width, self.leftView.frame.height) / 2
        
        self.rightView.frame = CGRect.init(x: self.frame.width / 2 - correction, y: 0, width: self.frame.width / 2 + correction, height: height)
        self.rightView.center.y = self.frame.height / 2
        self.rightView.layer.cornerRadius = min(self.leftView.frame.width, self.leftView.frame.height) / 2
    }
    
    private func animate(animations: @escaping (() -> Void)) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [.beginFromCurrentState, .curveEaseOut], animations: {
            animations()
        })
    }
    
    enum Style {
        case arrow
        case line
    }
}
