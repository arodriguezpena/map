//
//  INXPinView.swift
//  OnePay
//
//  Created by fyarad on 08-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit
import FlexLayout

class INXPinView: UIView {

    var labels: [UILabel] = []
            
    convenience init(length: Int) {
        self.init()
        self.flex.addCard().padding(.padding).define { (flex) in
                
            let size = ((.screenWidth - (.paddingMedium * 2)) / CGFloat(length))

            for i in 0...(length-1) {
                    
                let label = UILabel()
                label.backgroundColor = .clear
                label.text = "●"
                label.textColor = .dynamicLabelTertiary
                label.textAlignment = .center
                label.tag = i
                label.font = UIFont.systemFont(ofSize: 22, weight: .bold)
                label.adjustsFontSizeToFitWidth = true
                flex.addItem(label).width(size)
            
                self.labels.append(label)

            }

        }.shrink(1)
        
    }
         
    @objc func setPinLength(_ length: Int) {
        
        for i in 0...(self.labels.count - 1) {
            self.labels[i].textColor = (i + 1 > length) ? .dynamicLabelTertiary : .purpleColor
        }

    }
    
    
}
