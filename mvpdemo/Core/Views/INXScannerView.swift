//
//  INXScannerView.swift
//  OnePay
//
//  Created by fyarad on 08-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit
import AVFoundation

typealias ScanningFound = (_ code: String) -> Void

class INXScannerView: UIView, AVCaptureMetadataOutputObjectsDelegate {
    
    var captureSession = AVCaptureSession()
    var previewLayer: AVCaptureVideoPreviewLayer?
    var processingResult: Bool = false
    var parentController: UIViewController?
    var foundBlock: ScanningFound?
    
    convenience init(parentController: UIViewController, foundBlock: ScanningFound?) {
        self.init()
        
        self.foundBlock = foundBlock
        self.parentController = parentController
        
        if let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) {
            
            var input: AVCaptureDeviceInput!
            
            do {
                input = try AVCaptureDeviceInput(device: device)
            } catch {
                return
            }
            
            if (self.captureSession.canAddInput(input)) {
                self.captureSession.addInput(input)
            } else {
                failed()
                return
            }
            
            let output = AVCaptureMetadataOutput()
            
            if (self.captureSession.canAddOutput(output)) {
                output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                self.captureSession.addOutput(output)
                
                output.metadataObjectTypes = output.availableMetadataObjectTypes
            } else {
                failed()
                return
            }
            
            self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
            self.previewLayer?.videoGravity = .resizeAspectFill
            if let layer = self.previewLayer {
                self.layer.addSublayer(layer)
            }
            
            self.captureSession.startRunning()
        }
        
        
    }
    
    override var frame: CGRect {
        didSet {
            self.previewLayer?.frame = self.bounds
        }
    }
    
    func failed() {
        let alert = UIAlertController(title: "Escaneo no disponible", message: "Este dispositivo no soporta la lectura de códigos QR.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        self.parentController?.present(alert, animated: true)
    }
    
    func viewWillAppear() {
        if (self.captureSession.isRunning == false) {
            self.captureSession.startRunning()
        }
    }
    
    func viewWillDisappear() {
        if (self.captureSession.isRunning == true) {
            self.captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if let result = (metadataObjects.first as? AVMetadataMachineReadableCodeObject)?.stringValue, let block = self.foundBlock, self.processingResult == false {
            self.processingResult = true

            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            block(result)
        }
 
    }
    
    func toggleFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
}
