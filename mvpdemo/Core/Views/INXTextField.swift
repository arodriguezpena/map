//
//  INXTextField.swift
//  OnePay
//
//  Created by fyarad on 06-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import FontAwesome_swift
import FlexLayout

enum TextFieldType { case text, phone, verificationCode, email, name, password, custom}

typealias INXTextFieldValidateBlock = (_ text: String) -> Bool
typealias INXTextFieldValidationBlock = (_ text: String, _ valid: Bool) -> Void
typealias INXTextFieldReturn = (_ textField: UITextField) -> Void

class INXTextField: UIView, UITextFieldDelegate {

    let placeholderLabel = UILabel()
    let textField = UITextField()
    let divider = UIView()
    let errorLabel = UILabel()
    
    var type = TextFieldType.text
    var validateBlock: INXTextFieldValidateBlock?
    var validationBlock: INXTextFieldValidationBlock?
    var onReturn: INXTextFieldReturn?
    var shouldValidate = true
    var statusIcon = UIImageView()
    
    var valid: Bool = false {
        didSet {
            if self.type == .password {
                
            }
            else if (self.text.count == 0 || self.shouldValidate == false || self.valid == false) {
                self.statusIcon.image = nil
            }
            else if (self.valid == true) {
                self.statusIcon.image = UIImage.fontAwesomeIcon(name: .check, style: .solid, textColor: .greenColor, size: CGSize(width: 24, height: 24))
            }

        }
    }
    
    var text: String {
        get {
            return self.textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        }
        set(value) {
            self.textField.text = value
            self.textDidChange()
        }
    }
    
    convenience init(placeholder: String, errorLabel: String = "Campo inválido", type: TextFieldType) {
        self.init()
        self.type = type
        self.flex.height(68).width(100%)

        self.backgroundColor = .clear
        
        self.placeholderLabel.text = placeholder
        self.addSubview(self.placeholderLabel)

        self.textField.backgroundColor = .clear
        self.textField.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        self.textField.textColor = .dynamicLabel
        self.textField.delegate = self
        self.textField.autocorrectionType = .no
        self.textField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        self.textField.autocapitalizationType = .none
        self.addSubview(self.textField)
        
        self.divider.backgroundColor = .divider
        self.addSubview(self.divider)
        
        if (type == .name) {
            self.textField.autocapitalizationType = .words
            self.textField.textContentType = .name
        }
        else if (type == .email) {
            self.textField.keyboardType = .emailAddress
            self.textField.textContentType = .emailAddress
        }
        else if (type == .password) {
            self.statusIcon.flex.addTap(target: self, selector: #selector(passwordEyeAction))
            self.passwordEyeAction()
            self.shouldValidate = false
        }
        else if (type == .phone) {
            self.textField.keyboardType = .phonePad
            self.textField.textContentType = .telephoneNumber
        }
        else if (type == .verificationCode) {
            self.textField.keyboardType = .phonePad
            self.shouldValidate = false
            if #available(iOS 12.0, *) {
                self.textField.textContentType = .oneTimeCode
            }
        }
        else if (type == .text) {
            self.shouldValidate = false
        }
        
        self.statusIcon.contentMode = .center
        self.addSubview(self.statusIcon)
        
        self.errorLabel.textColor = .redColor
        self.errorLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        self.addSubview(self.errorLabel)
        
        self.setEditing(false, animated: false)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setEditing(self.textField.isFirstResponder)
        self.textField.frame = CGRect(x: 0, y: 16, width: self.width - 24, height: 34)
        self.errorLabel.frame = CGRect(x: 0, y: self.divider.yDiff + 4, width: self.width, height: 14)
        self.statusIcon.frame = CGRect(x: self.textField.xDiff, y: 0, width: 24, height: 50)
    }
    
    func startEditing() {
        self.textField.becomeFirstResponder()
    }
    
    func validOrFocus() -> Bool {
        if self.valid == false {
            self.startEditing()
        }
        return self.valid
    }
    
    @objc func textDidChange() {
        
        if let validateBlock = self.validateBlock {
            self.valid = validateBlock(self.text)
        }
        else if (self.type == .email) {
            self.valid = NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}").evaluate(with: self.text)
        }
        else if (self.type == .password) {
            self.valid = //NSPredicate(format:"SELF MATCHES %@", ".*[A-Z].*").evaluate(with: self.text) &&
                //NSPredicate(format:"SELF MATCHES %@", ".*[a-zA-Z].*").evaluate(with: self.text) &&
                //NSPredicate(format:"SELF MATCHES %@", ".*\\d.*").evaluate(with: self.text) &&
                self.text.count >= 8 && self.text.count <= 64
        }
        else if (self.type == .name) {
            self.valid = (self.text.count >= 2)
        }
                
        self.validationBlock?(self.textField.text ?? "", self.valid)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.setEditing(true, animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.setEditing(false, animated: true)
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let block = self.onReturn {
            block(textField)
        }
        else {
            textField.resignFirstResponder()
        }
        return true
    }
        
    func setEditing(_ editing: Bool, animated: Bool = false) {
        
        let showError = (editing == false && self.valid == false && self.text.count > 0 && self.shouldValidate == true)
        
        let changeBlock: (() -> Void) = {
            self.placeholderLabel.frame = CGRect(x: 0, y: 0, width: self.width, height: (editing || self.text.count > 0 ? 16 : 50))
            self.placeholderLabel.font = UIFont.systemFont(ofSize: (editing || self.text.count > 0 ? 12 : 16), weight: .regular)
            self.placeholderLabel.textColor = (editing ? .purpleColor : .dynamicLabelTertiary)
            self.divider.backgroundColor = (editing ? .purpleColor : (showError ? .redColor : .divider))
            self.divider.frame = CGRect(x: 0, y: 50 - (editing || showError ? 2 : 1), width: self.width, height: (editing || showError ? 2 : 1))
            self.errorLabel.alpha = (showError ? 1.0 : 0.0)
        }
        
        if (animated) {
            
            UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
                changeBlock()
            })
            
        }
        else {
            changeBlock()
        }
        
        
    }
    
    @objc func passwordEyeAction() {
        self.textField.isSecureTextEntry = !self.textField.isSecureTextEntry
        self.statusIcon.image = UIImage.fontAwesomeIcon(name: (self.textField.isSecureTextEntry ? .eye : .eyeSlash), style: .solid, textColor: .dynamicLabelTertiary, size: CGSize(width: 24, height: 24))
    }

    
}
