//
//  INXButton.swift
//  OnePay
//
//  Created by fyarad on 06-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit
import AttributedStringBuilder
import FlexLayout

class INXButton: UIButton {

    convenience init(title: String, style: INXGradientStyle = .yellow, target: Any? = nil, selector: Selector? = nil) {
        self.init()

        self.setBackgroundImage(UIColor.image(color: (style == .yellow ? .yellowColor : .purpleColor)), for: .normal)
        self.setAttributedTitle(AttributedStringBuilder().text(title, attributes: [
            .font(UIFont.systemFont(ofSize: 14, weight: .bold)),
            .textColor((style == .yellow ? .purpleColor : .white)),
        ]).attributedString, for: .normal)

        self.setBackgroundImage(UIColor.image(color: .backgroundLight), for: .disabled)
        self.setAttributedTitle(AttributedStringBuilder().text(title, attributes: [
            .font(UIFont.systemFont(ofSize: 14, weight: .bold)),
            .textColor(.dynamicLabelTertiary),
        ]).attributedString, for: .disabled)

        self.cornerRadius = .paddingSmall
        self.flex.height(50).paddingHorizontal(.paddingBig).width(100%)

        if let actionTarget = target, let actionSelector = selector {
            self.addTarget(actionTarget, action: actionSelector, for: .touchUpInside)
        }
        
    }
    
    

}
