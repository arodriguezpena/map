//
//  INXNavigationController.swift
//  OnePay
//
//  Created by fyarad on 07-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit

class INXNavigationController: UINavigationController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarHidden(true, animated: animated)
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        viewController.hidesBottomBarWhenPushed = true
        super.pushViewController(viewController, animated: animated)
        if self.viewControllers.count <= 1 {
            viewController.hidesBottomBarWhenPushed = false
        }
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        if self.viewControllers.count <= 2 {
            self.viewControllers.first?.hidesBottomBarWhenPushed = false
        }
        return super.popViewController(animated: animated)
    }

}
