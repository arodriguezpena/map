//
//  INXCardView.swift
//  OnePay
//
//  Created by fyarad on 07-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit
import FlexLayout

class INXCardView: UIView {

    let contentView = UIView()
    
    convenience init(direction: Flex.Direction) {
        self.init()
        self.backgroundColor = .clear
        self.applyShadow()
        self.flex.define { (flex) in
            
            self.contentView.backgroundColor = .white
            self.contentView.cornerRadius = .padding
            flex.addItem(self.contentView).direction(direction).alignItems(.center).justifyContent(.center).addPaddings().shrink(1)
            
        }
        
    }
    

  

}
