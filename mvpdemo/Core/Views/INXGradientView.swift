//
//  INXGradientView.swift
//  OnePay
//
//  Created by fyarad on 06-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit

enum INXGradientStyle {
    case yellow, purple, disabled
}

class INXGradientView: UIView {
    
    let gradientLayer = CAGradientLayer()
    
    convenience init(style: INXGradientStyle) {
        if style == .purple {
            self.init(startColor: .purpleGradientStart, endColor: .purpleGradientEnd)
        }
        else if style == .yellow {
            self.init(startColor: .yellowGradientStart, endColor: .yellowGradientEnd)
        }
        else {
            self.init(startColor: .backgroundLight, endColor: .backgroundLight)
        }
    }
    
    convenience init(startColor: UIColor, endColor: UIColor) {
        self.init()
        self.gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        self.gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.setColors(startColor: startColor, endColor: endColor)
        self.layer.insertSublayer(self.gradientLayer, at: 0)
    }
    
    func setColors(startColor: UIColor, endColor: UIColor) {
        self.gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.gradientLayer.frame = self.bounds
    }
    
}
