//
//  INXTitleView.swift
//  OnePay
//
//  Created by fyarad on 07-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit

class INXTitleView: INXGradientView {

    var parentController: UIViewController?
    var showCancel = false
    
    convenience init(_ title: String, description: String? = nil, showCancel: Bool = false, parentController: UIViewController? = nil) {
        self.init(style: .yellow)
        self.parentController = parentController
        self.showCancel = showCancel
        self.flex.addPaddings().define { (flex) in
                                    
            flex.addItem().direction(.row).alignItems(.center).define { (flex) in

                if parentController?.navigationController?.viewControllers.count == 1 {
                    flex.addItem(UIImageView(image: UIImage.fontAwesomeIcon(name: .chevronLeft, style: .solid, textColor: .purpleColor, size: CGSize(width: 24, height: 24)))).marginRight(.paddingSmall)
                }
                else if parentController?.navigationController?.viewControllers.count ?? 0 > 1 {
                    flex.addItem(UIImageView(image: UIImage.fontAwesomeIcon(name: .chevronLeft, style: .solid, textColor: .purpleColor, size: CGSize(width: 24, height: 24)))).marginRight(.paddingSmall)
                }

                flex.addItem(UILabel(title, attributes: [
                    .font(UIFont.systemFont(ofSize: 20, weight: .regular)),
                    .textColor(.purpleColor)
                ]))

            }.addTap(target: self, selector: #selector(backAction))
            
            if let descriptionString = description {
                flex.addItem(UILabel(descriptionString, type: .regular16, textColor: .black)).marginTop(.padding)
            }

        }.paddingTop(.statusBarHeight + .paddingBig)
        
    }

    @objc func backAction() {
        
        if self.showCancel == true || self.parentController?.navigationController?.viewControllers.count == 1 {
            self.parentController?.dismiss(animated: true, completion: nil)
        }
        else if self.parentController?.navigationController?.viewControllers.count ?? 0 > 1 {
            self.parentController?.navigationController?.popViewController(animated: true)
        }

    }
    
}
