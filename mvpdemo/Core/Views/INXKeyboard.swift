//
//  INXKeyboard.swift
//  OnePay
//
//  Created by fyarad on 08-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit
import FlexLayout
import AttributedStringBuilder

typealias INXKeyboardChange = (_ value: String) -> Void

class INXKeyboard: UIView {

    var changeBlock: INXKeyboardChange?
    var value = ""
    
    convenience init(changeBlock: INXKeyboardChange?) {
        self.init()
        
        self.changeBlock = changeBlock
        self.backgroundColor = UIColor.hex("#d0d1d9")
                
        let values: [[String]] = [["1", "2", "3"], ["4", "5", "6"], ["7", "8", "9"], ["", "0", "delete"]]
        let margin: CGFloat = 6
        
        self.flex.padding(margin).define { (flex) in
            
            flex.addItem().define { (flex) in
            
                values.forEach { (row) in
                    
                    flex.addItem().direction(.row).define { (flex) in
                        
                        row.forEach { (key) in
                            
                            flex.addItem().alignItems(.center).justifyContent(.center).padding(margin/2).define { (flex) in

                                let button = UIButton()
                                button.restorationIdentifier = key
                                button.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
                                button.cornerRadius = 5

                                if key == "delete" {
                                    button.setImage(UIImage.fontAwesomeIcon(name: .backspace, style: .solid, textColor: .black, size: CGSize(width: 24, height: 24)), for: .normal)
                                }
                                else if key.count > 0 {

                                    button.setAttributedTitle(AttributedStringBuilder().text(key, attributes: [
                                        .font(UIFont.systemFont(ofSize: 22, weight: .regular)),
                                        .textColor(.black)
                                    ]).attributedString, for: .normal)
                                    button.setBackgroundImage(UIColor.image(color: UIColor.hex("#fffdff")), for: .normal)

                                }

                                flex.addItem(button).height(48).width(100%)

                            }.width(33%)
                            
                        }
                        
                    }
                    
                }
                
                
            }.marginBottom(.bottomSafe + .paddingBig)
            
            
            
        }
        
    }
    
    @objc private func buttonClicked(sender: UIButton) {
        if let key = sender.restorationIdentifier {
            
            if key == "delete" {
                self.value = String(self.value.dropLast())
            }
            else {
                self.value += key
            }
            
            self.changeBlock?(self.value)
        }
    }


}
