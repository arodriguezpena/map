//
//  Notification.swift
//  OnePay
//
//  Created by fyarad on 09-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit

enum NotificationType: String {
    case addedPaymentMethod
    case updateTransactions
    case loadPendingTransactions
    case newTransaction
    case sessionTimeWarning
}

extension Notification {
    
    static func notify(_ name: NotificationType) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: name.rawValue), object: nil)
    }

    static func subscribe(_ observer: Any, selector: Selector, name: NotificationType) {
        NotificationCenter.default.addObserver(observer, selector: selector, name: Notification.Name(rawValue: name.rawValue), object: nil)
    }
    
    static func unsubscribe(_ observer: Any, name: NotificationType? = nil) {
        if let notificationName = name {
            NotificationCenter.default.removeObserver(observer, name: Notification.Name(rawValue: notificationName.rawValue), object: nil)
        }
        else {
            NotificationCenter.default.removeObserver(observer, name: nil, object: nil)
        }
    }
    
}
