//
//  UIView.swift
//  OnePay
//
//  Created by fyarad on 06-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit

public extension UIView {
    
    var topCornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set(value) {
            self.clipsToBounds = true
            self.layer.cornerRadius = value
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }

    var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set(value) {
            self.clipsToBounds = true
            self.layer.cornerRadius = value
        }
    }

    func setBorderColor(color: UIColor, _ width: CGFloat = 1.0) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    func setSize(_ size: CGSize) {
        var frame = self.frame
        frame.size = size
        self.frame = frame
    }
    
    func setOrigin(_ origin: CGPoint) {
        var frame = self.frame
        frame.origin = origin
        self.frame = frame
    }
    
    func setOriginX(_ x: CGFloat) {
        var frame = self.frame
        frame.origin.x = x
        self.frame = frame
    }
    
    func setOriginY(_ y: CGFloat) {
        var frame = self.frame
        frame.origin.y = y
        self.frame = frame
    }
    
    @objc func setWidth(_ width: CGFloat) {
        var frame = self.frame
        frame.size.width = width
        self.frame = frame
    }
    
    func setHeight(_ height: CGFloat = 0) {
        var frame = self.frame
        frame.size.height = height
        self.frame = frame
    }
    
    var xDiff: CGFloat {
        return self.frame.origin.x + self.frame.size.width
    }
    
    var yDiff: CGFloat {
        return self.frame.origin.y + self.frame.size.height
    }
    
    var width: CGFloat {
        return self.frame.size.width
    }
    
    var height: CGFloat {
        return self.frame.size.height
    }

    func applyShadow(color: UIColor = .black, alpha: Float = 0.15, x: CGFloat = 6, y: CGFloat = 6, blur: CGFloat = 12) {
        self.clipsToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = alpha
        self.layer.shadowOffset = CGSize(width: x, height: y)
        self.layer.shadowRadius = blur / 2.0
        self.layer.shadowPath = nil
    }
    
}

