//
//  UILabel.swift
//  OnePay
//
//  Created by fyarad on 06-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit
import AttributedStringBuilder

enum UILabelStyle {
    
    case h1, h2, h3, h4, h5, h6, body, span, placeholder, link, link16, regular16
    
    var attributes: [AttributedStringBuilder.Attribute] {
        switch self {
        case .h1:
            return [.font(.systemFont(ofSize: 36, weight: .bold)), .textColor(.dynamicLabel)]
        case .h2:
            return [.font(.systemFont(ofSize: 22, weight: .bold)), .textColor(.dynamicLabel)]
        case .h3:
            return [.font(.systemFont(ofSize: 18, weight: .bold)), .textColor(.dynamicLabel)]
        case .h4:
            return [.font(.systemFont(ofSize: 16, weight: .bold)), .textColor(.dynamicLabel)]
        case .h5:
            return [.font(.systemFont(ofSize: 14, weight: .bold)), .textColor(.dynamicLabel)]
        case .h6:
            return [.font(.systemFont(ofSize: 12, weight: .bold)), .textColor(.dynamicLabel)]
        case .span:
            return [.font(.systemFont(ofSize: 12, weight: .regular)), .textColor(.dynamicLabelSecondary), .lineSpacing(4)]
        case .placeholder:
            return [.font(.systemFont(ofSize: 12, weight: .regular)), .textColor(.dynamicLabelTertiary)]
        case .link:
            return [.font(.systemFont(ofSize: 14, weight: .regular)), .textColor(.purpleColor), .lineSpacing(4), .underline(true)]
        case .link16:
            return [.font(.systemFont(ofSize: 16, weight: .regular)), .textColor(.purpleColor), .lineSpacing(4), .underline(true)]
        case .regular16:
            return [.font(.systemFont(ofSize: 16, weight: .regular)), .textColor(.dynamicLabelSecondary), .lineSpacing(4)]
        default:
            return [.font(.systemFont(ofSize: 14, weight: .regular)), .textColor(.dynamicLabel)]
        }
    }
    
}

struct UILabelContent {
    
    var string: String?
    var attributes: [AttributedStringBuilder.Attribute]
    var insertNewLine = false
    
    static func text(_ string: String?, attributes: [AttributedStringBuilder.Attribute] = [], insertNewLine: Bool? = false) -> UILabelContent {
        return self.init(string: string, attributes: attributes, insertNewLine: insertNewLine ?? false)
    }
    
    static func text(_ string: String?, style: UILabelStyle = .body, textColor: UIColor? = nil, textAlignment: NSTextAlignment = .left, insertNewLine: Bool = false) -> UILabelContent {
        var attributes = style.attributes
        attributes.append(.alignment(textAlignment))

        if let color = textColor {
            attributes.append(.textColor(color))
        }
        
        return self.init(string: string, attributes: attributes, insertNewLine: insertNewLine)
    }
    
}

extension UILabel {
    
    convenience init(_ text: String?, type: UILabelStyle = .body) {
        self.init()
        self.set(text, type: type)
    }
    
    convenience init(_ text: String?, type: UILabelStyle = .body, textColor: UIColor? = nil, textAlignment: NSTextAlignment = .left) {
        self.init(contents: [.text(text, style: type, textColor: textColor, textAlignment: textAlignment)])
    }
    

    convenience init(_ text: String?, attributes: [AttributedStringBuilder.Attribute]) {
        self.init()
        self.set(contents: [.text(text, attributes: attributes)])
    }
            
    func set(_ text: String?, type: UILabelStyle = .body, textColor: UIColor? = nil, textAlignment: NSTextAlignment = .left) {
        self.set(contents: [.text(text, style: type, textColor: textColor, textAlignment: textAlignment)])
    }

    func set(_ text: String?, attributes: [AttributedStringBuilder.Attribute]) {
        self.set(contents: [.text(text, attributes: attributes)])
    }
    
    convenience init(contents: [UILabelContent]) {
        self.init()
        self.set(contents: contents)
    }

    func set(contents: [UILabelContent]) {
        self.backgroundColor = UIColor.clear
        self.numberOfLines = 0
        self.isUserInteractionEnabled = true
        
        let builder = AttributedStringBuilder()
        
        contents.forEach { (content: UILabelContent) in
            
            if (content.string != nil && content.string?.count ?? 0 > 0) {
                
                if (content.insertNewLine) {
                    builder.newline()
                }
                
                builder.text(content.string ?? "", attributes: content.attributes)
                
            }
            
        }
        
        self.attributedText = builder.attributedString
        
    }
    
    func setAttributedText(text :String?, font: UIFont, color: UIColor, letterSpacing: CGFloat, lineHeight: CGFloat, alignment: NSTextAlignment) {
        
        let attributes = AttributedStringBuilder()
        attributes.defaultAttributes = [
            .font(font),
            .textColor(color),
            .alignment(alignment),
            .kerning(letterSpacing),
            .lineSpacing(lineHeight),
        ]
        
        attributes.text(text!)
        
        self.attributedText = attributes.attributedString
        
    }
    
    func updateWidth() {
        self.setWidth(self.getWidth())
    }
    
    func updateHeight() {
        self.numberOfLines = 0
        self.setHeight(self.getHeight())
    }
    
    func getWidth() -> CGFloat {
        let sizeForWidth = CGSize(width: CGFloat.greatestFiniteMagnitude / 2, height: CGFloat.greatestFiniteMagnitude / 2)
        let width = attributedText?.boundingRect(with: sizeForWidth, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).width
        return roundTillHalf(value: width ?? 0)
    }
    
    func getHeight() -> CGFloat {
        let sizeForHeight = CGSize(width: frame.size.width, height: CGFloat.greatestFiniteMagnitude / 2)
        let height = attributedText?.boundingRect(with: sizeForHeight, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).height
        return roundTillHalf(value: height ?? 0)
    }
    
    private func roundTillHalf(value: CGFloat) -> CGFloat {
        if value - CGFloat(Int(value)) >= 0.5 {
            return CGFloat(Int(value)) + 1
        } else {
            return CGFloat(Int(value)) + 0.5
        }
    }
    
}
