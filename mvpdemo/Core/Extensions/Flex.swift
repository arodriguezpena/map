//
//  Flex.swift
//  OnePay
//
//  Created by fyarad on 06-02-20.
//  Copyright © 2020 Ionix Spa. All rights reserved.
//

import UIKit
import FlexLayout

extension Flex {
    
    @discardableResult
    func addDivider() -> Flex {
        self.addItem().width(100%).height(1).backgroundColor(.divider)
    }
    
    @discardableResult
    func alignCenter() -> Flex {
        self.alignItems(.center).justifyContent(.center)
    }

    @discardableResult
    func addImage(_ name: String) -> Flex {
        let image = UIImageView(image: UIImage(named: name))
        return self.addItem(image).aspectRatio(of: image)
    }

    @discardableResult
    func addTitle(_ title: String, description: String? = nil, showCancel: Bool = false, parentController: UIViewController? = nil) -> Flex {
        return self.addItem(INXTitleView(title, description: description, showCancel: showCancel, parentController: parentController))
    }

    @discardableResult
    func addPaddings() -> Flex {
        return self.paddingHorizontal(.paddingMedium).paddingVertical(.paddingBig)
    }

    @discardableResult
    func addCard(target: Any? = nil, selector: Selector? = nil) -> Flex {
        let cardView = INXCardView(direction: .row)
        self.addItem(cardView).addTap(target: target, selector: selector)
        return cardView.contentView.flex
    }

    @discardableResult
    func addTap(target: Any? = nil, selector: Selector? = nil) -> Flex {
        if let actionTarget = target, let actionSelector = selector {
            self.view?.isUserInteractionEnabled = true
            self.view?.addGestureRecognizer(UITapGestureRecognizer(target: actionTarget, action: actionSelector))
        }
        return self
    }
    
    @discardableResult
    func addNavButton(title: String, backIcon: Bool = false, color: UIColor = .dynamicLabelSecondary, target: Any? = nil, selector: Selector? = nil) -> Flex {
        return self.addItem().paddingTop(.paddingBig).paddingBottom(.paddingSmall).direction(.row).define { (flex) in
            
            if backIcon == true {
                flex.addItem(UIImageView(image: UIImage.fontAwesomeIcon(name: .chevronLeft, style: .solid, textColor: color, size: CGSize(width: 20, height: 20)))).marginRight(4)
            }
            
            flex.addItem(UILabel(title, attributes: [
                .font(UIFont.systemFont(ofSize: 16, weight: .regular)),
                .textColor(color),
            ]))
            
        }.addTap(target: target, selector: selector)
    }
    


}
